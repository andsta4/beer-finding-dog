from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.3.5',
    description='Program for finding best route for beer degustation.',
    author='andriusstankevicius',
    license='MIT',
    install_requires=[ 'Click' ],
    entry_points ='''
        [console_scripts]
        find_path=src.find_path:main
        make_data=src.make_data:main
    '''
)
