import pandas
import click
from src.Haversine import Haversine




# calculate distance from home coordinates to each in geocodes
def calculate_distance(current_pos, next_pos):
    return Haversine((current_pos['longitude'], current_pos['latitude']), (next_pos['longitude'], next_pos['latitude'])).km

def calculate_distances(dataset,current_pos):
    return dataset.apply(lambda row: (row['brewery_id'], calculate_distance(current_pos, row)), axis=1)


@click.command()
@click.argument('latitude')
@click.argument('longitude')
def main(latitude, longitude):
    # Coordinates of home.
    home = {'brewery_id': 0, 'longitude': float(longitude),
            'latitude': float(latitude), 'beers': '', 'beer_number': 0, 'distance': 0,
            'distance_to_home': 0}
    # Loading data.
    df = pandas.read_csv('data/processed/geocodes_and_beers.csv')
    fuel = 2000
    stops = []
    stops.append(home)
    come_back = False
    # Searching for best path.
    while not come_back:
        current_stop = stops[-1]
        distances_to_current = df.apply(lambda row: calculate_distance(current_stop, row), axis=1)
        if len(stops) == 1:
            df['distance_to_home'] = distances_to_current
        df['distance'] = distances_to_current
        closest_brews = df[df['distance'] < (fuel / 2)]
        sort_closest_brews = closest_brews.sort_values(by=['beer_number', 'distance'], ascending=[False, True])
        # Determining next location.
        try: # If there is no next location.
            next_stop = dict(sort_closest_brews.iloc[0, :])
            fuel -= next_stop['distance']
        except IndexError:
            stops.append(home)
            break

        if fuel <= 0 or next_stop['distance_to_home'] > fuel:
            stops.append(home)
            fuel += next_stop['distance']
            come_back = True
        if not come_back:
            stops.append(next_stop)
            # Remove that brewery from dataset.
            df = df[df.brewery_id != next_stop['brewery_id']]

    # Print factories list
    print('Found %d beer factories' % (len(stops) - 2))
    start = True
    for factory in stops:
        if factory['brewery_id'] == 0:
            if start:
                print('\t-> HOME: %s,%s distance %dkm' % (factory['latitude'], factory['longitude'],
                                                          factory['distance']))
            start = False
            continue
        print('\t-> [%s]: %s,%s distance %dkm' % (factory['brewery_id'], factory['latitude'],
                                                  factory['longitude'], factory['distance']))
    last_distance = calculate_distance(stops[-2], stops[-1])
    total_traveled = (2000 - fuel + last_distance)
    print('\t<- HOME: %s,%s distance %dkm' % (stops[-1]['latitude'], stops[-1]['longitude'],
                                              last_distance))
    print('Total distance travelled: %dkm' % (total_traveled))

    # Print beers list
    beers = []
    for item in stops:
        beers += item['beers'].split(';')
    beers = list(filter(None, beers))
    print('Collected %d beers:' % (len(beers)))
    for beer in beers:
        print('\t-> %s' % (beer))

if __name__ == '__main__':
    main()
