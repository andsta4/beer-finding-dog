import pandas
import click


def add_beers_to_dataset(beers_df, geocodes_df):
    # adding beers to geocodes db
    for row in beers_df.itertuples(index=True, name=None):
        # Getting index as an int type number.
        try: # Can be that beer is not in any of known breweries
            indx = geocodes_df.index[(geocodes_df['brewery_id']==row[2])].to_list()[0]
        except IndexError:
            # There is no known brewery that makes this kind of beer or data is inaccurate.
            continue # Don't add any information
        # Adding a beer name.
        geocodes_df.iloc[indx, 3] += "%s;" % (row[3])
        # Adding one to the beer column.
        geocodes_df.iloc[indx, 4] += 1


@click.command()
def main():
    geocodes_df = pandas.read_csv('data/raw/geocodes.csv')
    geocodes_df = geocodes_df[['brewery_id', 'latitude', 'longitude']]
    beers_df = pandas.read_csv("data/raw/beers.csv")
    geocodes_df['beers'] = ""
    geocodes_df['beer_number'] = 0
    add_beers_to_dataset(beers_df, geocodes_df)
    # Remove breweries that make 0 known beers
    df = geocodes_df[geocodes_df['beer_number'] != 0]
    df.to_csv('data/processed/geocodes_and_beers.csv', index=False)


if __name__ == '__main__':
    main()
