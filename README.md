beer-finding-dog
==============================

Program for finding best route for beer degustation.

Setup:

'virtualenv venv'
'source venv/bin/activate'
'pip install -e .'

Usage:

- To make dataset used for finding best path
'make_data'

- To find best path
'find_path latitude longitude'

Example:
'find_path 53.11 10.22'

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
